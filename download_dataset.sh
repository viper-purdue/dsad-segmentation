#!/bin/bash

# 定义要保存文件的目录
DIR="data/DSAD"

# 确保目标目录存在
mkdir -p $DIR

# 定义要下载和解压的文件名
FILE_NAME="DSAD.zip"

# 下载文件
wget -O "$DIR/$FILE_NAME" "https://springernature.figshare.com/ndownloader/files/38494425"

# 检查 wget 命令是否成功执行
if [ $? -eq 0 ]; then
  # 解压文件
  unzip "$DIR/$FILE_NAME" -d "$DIR"

  # 检查 unzip 命令是否成功执行
  if [ $? -eq 0 ]; then
    # 删除 zip 文件
    rm "$DIR/$FILE_NAME"
  else
    echo "Failed to unzip $FILE_NAME"
  fi
else
  echo "Failed to download $FILE_NAME"
fi
