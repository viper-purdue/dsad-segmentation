CUDA_ID="0"  # change this to the GPU id you use
export CUDA_VISIBLE_DEVICES=$CUDA_ID 
python tools/train.py configs/segformer/segformer_dsad_sup.py --work-dir work_dir/sup/abdominal_wall