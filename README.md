Create the virtual environment

```python
#Create the virtual environment:(python=3.8.18, CUDA=12.1 required). Do this in your Linux terminal and set the Jupyter Notebook Tutorial.ipynb's kernel to be the environment you just created
!conda env create -f environment.yml#(will install pytorch=2.2.0)
!conda activate openmmlab
#If you fail to install mmcv, follow this guide https://github.com/open-mmlab/mmsegmentation/blob/main/docs/en/get_started.md#installation
```

Prepare datasets

```python
#Download the dataset(Dresden Surgical Anatomy Dataset)
!bash mmsegmentation/download_dataset.sh
#To convert the original dataset into the supplemented dataset:
#Run data/convert_dataset.ipynb, generate two new folders /mmsegmentation/data/DSAD_orig /mmsegmentation/data/DSAD_sup
```

Training and evaluation

```python
#Train models on abdominal wall supplemented subset with fore-background weights of 1.5:1:
!bash mmsegmentation/train_segformer_sup_abdominal_wall.sh

#If you want to modify training dataset (to other organs, or to the original dataset), change path in mmsegmentation/configs/_base_/datasets/dsad_sup.py

#if you want to modify fore-background weights, change class_weights in mmsegmentation/configs/_base_/models/segformer_dsad_sup.py
```

```python
#Test models on abdominal wall supplemented subset
!bash test.sh
#If you want to modify test dataset (to other organs, or to the original dataset), change path in mmsegmentation/configs/_base_/datasets/dsad_sup.py
#If you want to modify the model to test, change model path in test.sh
```